import { Component, OnInit } from '@angular/core';
import { TableData, TableSettings } from './service';
import { FormView } from '../../../@components/form/form.service';
import { DataService } from '../../../@components/data.service';

@Component({
  selector: 'ngb-dev-data',
  templateUrl: 'dev-data.component.html'
})
export class DevDataComponent implements OnInit {
  data: any[];
  settings: any;
  searchMethod: any;
  form = new FormView();
  formElements: any;

  constructor(private server: DataService) {
    this.form.panel = false;
  }

  ngOnInit() {
    this.data = TableData;
    this.getFormElements();
    this.getDataListConfig();
    this.form.panel = true;
  }

  /**
   * Get form elements from server
   */
  getFormElements() {
    this.server
      .getAnyData('Data-list form elements', 'api/data-list-form')
      .subscribe(result => {
        this.formElements = result;
      });
  }

  /**
   * Get data-list config from server
   */
  getDataListConfig() {
    this.server
      .getAnyData('Data-list config', 'api/data-list-config')
      .subscribe(result => {
        this.settings = result;
      });
  }

  /**
   * Methods
   */
  onCreateNew() {
    this.form.edit = null;
    this.form.show = true;
  }
}
