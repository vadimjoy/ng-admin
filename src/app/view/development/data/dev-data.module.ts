import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DevDataComponent } from './dev-data.component';
import { ComponentsModule } from '../../../@components/components.module';
// import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    // FormsModule
  ],
  declarations: [DevDataComponent]
})
export class DevDataModule {
}
