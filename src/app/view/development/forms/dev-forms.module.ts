import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DevFormsComponent } from './dev-forms.component';
import { ComponentsModule } from '../../../@components/components.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule
  ],
  declarations: [DevFormsComponent]
})
export class DevFormsModule {
}
