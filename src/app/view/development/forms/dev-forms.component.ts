import { Component, OnInit } from '@angular/core';
import { FormElements } from './form';
import { FormView } from '../../../@components/form/form.service';

@Component({
  selector: 'ngb-dev-forms',
  templateUrl: 'dev-forms.component.html',
  styleUrls: ['dev-forms.component.scss']
})
export class DevFormsComponent implements OnInit {
  elements: any[];
  panelView = false;
  formLayout = 'stacked';
  id = null;
  form = new FormView();

  ngOnInit() {
    this.elements = FormElements;
  }

  switchFormView() {
    this.panelView = !this.panelView;
  }

  onCreate() {
    alert('Created!');
  }

  onUpdate() {
    alert('Updated!');
  }

  onDelete() {
    alert('Deleted!');
  }
}
