const locale = {
  calendar: {
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    dayNamesLong: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    firstDayOfWeek: 1
  }
};

export const FormElements = [
  {
    /**
     * value: string
     */
    key: 'text',
    label: 'Name input',
    controlType: 'text',
    order: 0,
    required: true,
    tooltip: 'Text tooltip'
  },
  {
    /**
     * value: text
     */
    key: 'textarea',
    label: 'Textarea',
    controlType: 'textarea',
    order: 1,
    required: true,
    tooltip: 'Textarea tooltip',
  },
  {
    /**
     * value: key (array of keys)
     * options: { key:any, value:any }
     */
    key: 'select',
    label: 'Select',
    controlType: 'select',
    order: 2,
    required: true,
    tooltip: 'Select tooltip',
  },
  {
    /**
     * value: array of keys
     * options: { key:any, value:any }
     */
    key: 'select2',
    label: 'Select 2 multiple',
    placeholder: 'Select items',
    controlType: 'select2',
    order: 3,
    required: true,
    multiple: true,
    note: 'Selected items: ',
    tooltip: 'Select 2 tooltip',
  },
  {
    /**
     * value: key
     * options: { key:any, value:any }
     */
    key: 'select2s',
    label: 'Select 2 single',
    placeholder: 'Select items',
    controlType: 'select2',
    order: 4,
    required: true,
    multiple: false,
    note: 'Selected items: ',
    tooltip: 'Select 2s tooltip',
  },
  {
    /**
     * value: boolean
     */
    key: 'checkbox',
    label: 'Checkbox',
    controlType: 'checkbox',
    order: 5,
    required: true,
    tooltip: 'Checkbox tooltip',
  },
  {
    /**
     * value: boolean
     */
    key: 'toggle',
    label: 'Toggle',
    controlType: 'toggle',
    order: 6,
    required: true,
    tooltip: 'Toggle tooltip',
  },
  {
    /**
     * value: array of keys
     * options: { key:any, value:any }
     */
    key: 'radiogroup',
    label: 'Radiogroup',
    controlType: 'radiogroup',
    order: 7,
    required: true,
    tooltip: 'Radiogroup tooltip',
  },
  {
    /**
     * value: array of keys
     * options: { key:any, value:any }
     */
    key: 'radiogroup_alt',
    label: 'Radiogroup alternate',
    controlType: 'radiogroup-alt',
    order: 8,
    required: true,
    tooltip: 'Radiogroup alternate tooltip',
  },
  {
    /**
     * value: array of keys
     * options: { key:any, value:any }
     */
    key: 'checkboxgroup',
    label: 'Checkboxgroup',
    controlType: 'checkboxgroup',
    order: 9,
    required: true,
    tooltip: 'Checkboxgroup tooltip',
  },
  {
    /**
     * value: array of keys
     * options: { key:any, value:any }
     */
    key: 'checkboxgroup_alt',
    label: 'Checkboxgroup alternate',
    controlType: 'checkboxgroup-alt',
    order: 10,
    required: true,
    tooltip: 'Checkboxgroup alternate tooltip',
  },
  Object.assign({
    /**
     * value: Date
     */
    key: 'datepicker',
    label: 'Datepicker',
    controlType: 'datepicker',
    order: 10,
    required: true,
    tooltip: 'Datepicker tooltip',
  }, locale.calendar),
  {
    /**
     * value: key
     * options: { key:any, value:any }
     */
    key: 'timepicker',
    label: 'Timepicker',
    controlType: 'timepicker',
    order: 12,
    required: true,
    tooltip: 'Timepicker tooltip',
  },
  {
    /**
     * value: number
     */
    key: 'number',
    label: 'Number',
    controlType: 'number',
    order: 14,
    required: true,
    min: 0,
    max: 9,
    step: 0.5,
    tooltip: 'Number tooltip',
  }
];
