import { Component } from '@angular/core';

@Component({
  selector: 'ngb-view',
  template: `
    <ngb-layout>
      <router-outlet></router-outlet>
    </ngb-layout>
  `,
})

export class ViewComponent {
}
