import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewRoutingModule } from './view.routing.module';
import { ViewComponent } from './view.component';
import { ComponentsModule } from '../@components/components.module';
/**
 * Views
 **/
import { DevFormsModule } from './development/forms/dev-forms.module';
import { DevDataModule } from './development/data/dev-data.module';

const VIEWS = [
  DevFormsModule,
  DevDataModule
];

@NgModule({
  imports: [
    CommonModule,
    ViewRoutingModule,
    ComponentsModule,
    [...VIEWS]
  ],
  declarations: [ViewComponent]
})
export class ViewModule {
}
