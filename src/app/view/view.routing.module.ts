import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ViewComponent } from './view.component';
import { DevFormsComponent } from './development/forms/dev-forms.component';
import { DevDataComponent } from './development/data/dev-data.component';

const routes: Routes = [{
  path: '',
  component: ViewComponent,
  children: [
    {
      path: 'forms',
      component: DevFormsComponent
    },
    {
      path: 'data',
      component: DevDataComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewRoutingModule {
}
