import { Component } from '@angular/core';

@Component({
  selector: 'ngb-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {
}
