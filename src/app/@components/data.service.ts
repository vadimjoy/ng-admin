import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { backend } from '../backend';
import { LogService } from './log.service';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  translate: BehaviorSubject<any> = new BehaviorSubject([]);
  httpOptions = {
    json: {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }
  };

  constructor(private http: HttpClient, private log: LogService) {}

  /**
   * Get form by id
   * @param from
   * @param url
   * @param id
   */
  getForm(from, url, id): Observable<any> {
    return this.http
      .get(`${url}/${id}`)
      .pipe(catchError(this.log.error(from, `id`)));
  }

  /**
   * Get Form options
   * @param from
   * @param url
   */
  getFormOptions(from, url): Observable<any> {
    return this.http
      .options(url)
      .pipe(catchError(this.log.error(from, 'options', [])));
  }

  /**
   * Post Form
   * @param from
   * @param url
   * @param data
   */
  postForm(from, url: string, data: any): Observable<any> {
    return this.http.post(url, data, this.httpOptions.json).pipe(
      tap(result => this.log.success(from, 'add', result)),
      catchError(this.log.error(from, 'post'))
    );
  }

  /**
   * Put Form
   * @param from
   * @param url
   * @param data
   */
  putForm(from, url: string, data: any): Observable<any> {
    return this.http.put(url, data, this.httpOptions.json).pipe(
      tap(result => this.log.success(from, 'update', result)),
      catchError(this.log.error(from, 'put'))
    );
  }

  /**
   * Delete form
   * @param from
   * @param url
   */
  deleteForm(from, url: string): Observable<any> {
    return this.http.delete(url, this.httpOptions.json).pipe(
      tap(result => this.log.success(from, 'delete', result)),
      catchError(this.log.error(from, 'delete'))
    );
  }

  /**
   * Get translate
   */
  getTranslateElements(): Observable<any> {
    return this.http.get(backend.translate).pipe(
      tap(result => this.translate.next(result)),
      catchError(this.log.error('Translate', 'get'))
    );
  }

  /**
   * Get any data
   * @param from
   * @param url
   */
  getAnyData(from, url): Observable<any> {
    return this.http
      .get(url)
      .pipe(catchError(this.log.error(from, `get`)));
  }
}
