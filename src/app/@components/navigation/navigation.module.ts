import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NavigationComponent, MenuComponent} from './navigation.component';
import {NavigationService} from './navigation.service';
import {HttpClientModule} from '@angular/common/http';
import { DataService } from '../data.service';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    HttpClientModule
  ],
  exports: [
    NavigationComponent,
    MenuComponent
  ],
  declarations: [
    NavigationComponent,
    MenuComponent
  ],
  providers: [
    DataService,
    NavigationService
  ]
})
export class NavigationModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: NavigationModule,
      providers: [
        NavigationService
      ],
    };
  }
}
