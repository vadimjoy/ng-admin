import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { DataService } from '../data.service';
import { Menu, NavigationService } from './navigation.service';
import { NavigationEnd, Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { filter } from 'rxjs/operators';

@Component({
  selector: '[ngb-menu]',
  templateUrl: './navigation.component.html'
})
export class MenuComponent {
  @Input() item: any;
  @Output() itemClick = new EventEmitter<any>();

  onItemClick(item) {
    this.itemClick.emit(item);
    item.is_active = !item.is_active;
  }
}

@Component({
  selector: 'ngb-navigation',
  template: `
    <nav class="nav">
      <ul class="nav__list">
        <li ngb-menu class="nav__list-item" *ngFor="let item of nav;"
            [item]="item"></li>
      </ul>
    </nav>`
})

export class NavigationComponent implements OnInit, OnDestroy, AfterViewInit {
  nav: Menu[];
  private alive: boolean;

  @ViewChildren(MenuComponent) sub: QueryList<MenuComponent>;

  constructor(private server: DataService, private navService: NavigationService, private router: Router) {
  }

  ngOnInit() {
    this.getNavMenu();
    this.onRouterEvent();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  ngAfterViewInit() {
    this.onSubMenuChanges();
  }

  private getNavMenu() {
    this.server.translate
      .filter((translate) => translate['navigation'])
      .subscribe((translate) => {
        this.nav = translate.navigation;
        this.prepareItems();
      });
  }

  private onSubMenuChanges() {
    this.sub.changes.subscribe(() => {
    });
  }

  private prepareItems() {
    this.navService.changeItems(this.nav, this.router.url);
  }

  private onRouterEvent() {
    this.router.events.pipe(
      takeWhile(() => this.alive),
      filter(e => e instanceof NavigationEnd),
    ).subscribe((e: NavigationEnd) => {
      this.navService.changeItems(this.nav, e.urlAfterRedirects);
    });
  }
}
