import { Injectable } from '@angular/core';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';

export abstract class Menu {
  /**
   * Name of menu-item
   * @type {string}
   */
  name: string;
  /**
   * Item relative link (for routerLink)
   * @type {string}
   */
  link?: string;
  /**
   * Submenu items
   * @type {array}
   */
  sub?: Menu[];
  /**
   * Parent item
   * @type {array}
   */
  parent?: Menu;
  /**
   * Active item of menu
   * @type {boolean}
   */
  is_active?: boolean;
}

@Injectable()
export class NavigationService {

  constructor() {
  }

  changeItems(items: Menu[], route: string, parent?: Menu) {
    this.eachItems(items, (item) => {
      if (parent) {
        item.parent = parent;
      }
      if (item.sub) {
        this.changeItems(item.sub, route, item);
      }
      if (item.link === route && parent) {
        this.expandItems(item.parent);
      }
    });
  }

  private expandItems(item: Menu) {
    item.is_active = true;
    if (item.parent) {
      this.expandItems(item.parent);
    }
  }

  private eachItems(items: Menu[], callback) {
    items.forEach((item: Menu) => {
      callback(item);
    });
  }
}
