import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  public warnings: Subject<any> = new Subject();
  public errors: Subject<any> = new Subject();
  public notes: Subject<any> = new Subject();

  constructor() { }

  private toArray(el: any) {
    const arr = [];
    if (typeof el === 'object' && el !== null && el !== undefined) {
      return el;
    } else if (el !== null && el !== undefined) {
      arr.push(el);
      return arr;
    } else {
      return arr;
    }
  }

  callWarning(warning: Array<string>) {
    this.warnings.next(this.toArray(warning));
  }

  callError(error: Array<string>) {
    this.errors.next(this.toArray(error));
  }

  callNote(note: Array<string>) {
    this.notes.next(this.toArray(note));
  }
}
