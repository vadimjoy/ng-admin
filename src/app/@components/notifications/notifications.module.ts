import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationsComponent } from './notifications.component';
import { NglModule } from 'ng-lightning/ng-lightning';
import { NotificationsService } from './notifications.service';

@NgModule({
  imports: [
    CommonModule,
    NglModule.forRoot()
  ],
  declarations: [
    NotificationsComponent
  ],
  exports: [
    NotificationsComponent
  ]
})
export class NotificationsModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: NotificationsModule,
      providers: [
        NotificationsService
      ],
    };
  }
}
