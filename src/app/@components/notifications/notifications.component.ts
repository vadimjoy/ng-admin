import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationsService } from './notifications.service';
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'ngb-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
  animations: [
    trigger('showHide', [
      transition(':enter', [
        style({transform: 'translateY(-100%)', opacity: 0}),
        animate('300ms', style({transform: 'translateY(0)', opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translateY(0)', opacity: 1}),
        animate('300ms', style({transform: 'translateY(-100%)', opacity: 0}))
      ])
    ])
  ]
})

export class NotificationsComponent implements OnInit, OnDestroy {
  warnings = [];
  errors = [];
  notes = [];
  constructor(private notifications: NotificationsService) { }

  ngOnInit() {
    this.notifications.warnings.subscribe(warnings => {
      this.warnings = warnings;
    });
    this.notifications.errors.subscribe(errors => {
      this.errors = errors;
    });
    this.notifications.notes.subscribe(notes => {
      this.notes = notes;
    });
  }

  ngOnDestroy() {
    this.notifications.warnings.unsubscribe();
    this.notifications.errors.unsubscribe();
    this.notifications.notes.unsubscribe();
  }

  animate(arr) {
  }

  clearWarning() {
    this.warnings = [];
  }

  clearError() {
    this.errors = [];
  }

  clearNote() {
    this.notes = [];
  }
}
