import {AfterViewInit, Directive, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, Renderer2} from '@angular/core';
import {Subject} from 'rxjs';

@Directive({
  selector: '[ngbCloseout]'
})
export class CloseoutDirective implements OnChanges, AfterViewInit, OnDestroy {
  @Input() open: any;
  @Output() openChange: EventEmitter<any> = new EventEmitter<any>();

  private events = <any>{};
  private clickEvent = null;
  private isOpen = new Subject();

  constructor(private element: ElementRef, private renderer: Renderer2) {
    this.onElementClick = this.onElementClick.bind(this);
    this.onGlobalClick = this.onGlobalClick.bind(this);
    this.onKeyEvent = this.onKeyEvent.bind(this);
  }

  ngOnChanges() {
    this.isOpen.next(this.open);
  }

  ngAfterViewInit() {
    this.isOpen.filter(value => value === true)
      .subscribe(() => {
        this.subscribeToEvents();
      });
    this.isOpen.filter(value => value === false)
      .subscribe(() => {
        this.unsubscribeFromEvents();
      });
  }

  ngOnDestroy() {
    this.isOpen.unsubscribe();
  }

  private subscribeToEvents() {
    this.events.elementClickEvents = this.renderer.listen(this.element.nativeElement, 'click', this.onElementClick);
    this.events.globalClickEvents = this.renderer.listen('document', 'click', this.onGlobalClick);
    this.events.globalKeyEvents = this.renderer.listen('document', 'keydown', this.onKeyEvent);
  }

  private onElementClick(e) {
    this.clickEvent = Date.now();
    return e.clickEvent = this.clickEvent;
  }

  private onGlobalClick(e) {
    if (e.clickEvent === this.clickEvent) {
      return;
    }
    this.closeOpenedElement();
  }

  private onKeyEvent(e) {
    if (e.keyCode === 9 || e.keyCode === 27) {
      this.closeOpenedElement();
    }
  }

  private unsubscribeFromEvents() {
    this.events.elementClickEvents();
    this.events.elementClickEvents = null;
    this.events.globalClickEvents();
    this.events.globalClickEvents = null;
    this.events.globalKeyEvents();
    this.events.globalKeyEvents = null;
  }

  private closeOpenedElement() {
    this.open = false;
    this.openChange.emit(this.open);
  }
}
