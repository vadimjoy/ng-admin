import { Injectable } from '@angular/core';
import { NotificationsService } from './notifications/notifications.service';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable({
  providedIn: 'root',
})
export class LogService {
  colors = {
    error: '#ff2b2b',
    warning: '#ffa812'
  };

  constructor(private notifications: NotificationsService) {
  }

  success(from, operation, result) {
    if (result.success && result.success.message) {
      this.notifications.callNote(result.success.message);
      this.note(from, operation, result.success.message);
    }
  }

  error<T>(from, operation = 'operation', result?: any) {
    return (error: any): Observable<T> => {
      result = result || { error: error.error };
      if (error instanceof Error) {
        this.callError(from, operation, error);
      } else {
        switch (error.status) {
          case 404:
            this.callWarning(from, operation, error);
            break;
          case 422:
            this.callWarning(from, operation, error);
            break;
          default: {
            this.callError(from, operation, error);
          }
        }
      }
      return of(result as T);
    };
  }


  private note(from, operation, message) {
    this.log('#555555', '#ffffff', from, operation, message);
    this.notifications.callNote(message);
  }

  private callWarning(from, operation, error) {
    const message = error.error.message || error.message;
    this.log('#ffb75d', '#3e3e3c', from, operation, message);
    this.notifications.callWarning(message);
  }

  private callError(from, operation, error) {
    const message = error.error.message || error.message;
    this.log('#ff2b2b', '#ffffff', from, operation, message);
    this.notifications.callError(message);
  }

  private log(background, color, from, operation, message) {
    return console.log(
      '%c%s', `background: ${background}; color: ${color}; padding: 0 2px; border-radius: 2px;`,
      `${from}: ${operation}`, message
    );
  }
}
