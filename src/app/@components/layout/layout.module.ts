import { LayoutComponent } from './layout.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NglModule } from 'ng-lightning/ng-lightning';
import { NavigationModule } from '../navigation/navigation.module';
import { NotificationsModule } from '../notifications/notifications.module';

@NgModule({
  imports: [
    CommonModule,
    NglModule.forRoot(),
    NavigationModule,
    NotificationsModule
  ],
  declarations: [
    LayoutComponent
  ],
  exports: [
    LayoutComponent
  ],
  providers: []
})

export class LayoutModule {
}
