import { Component } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'ngb-layout',
  templateUrl: './layout.component.html'
})
export class LayoutComponent {
  loaded = false;
  error = false;

  constructor(server: DataService) {
    server.getTranslateElements().subscribe(
      (result) => {
        result.error ? this.error = true : this.loaded = true;
      }
    );
  }
}
