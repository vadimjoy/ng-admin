import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LayoutModule } from './layout/layout.module';
import { FormModule } from './form/form.module';
import { DataListModule } from './data-list/data-list.module';

const MODULES = [
  FormModule,
  LayoutModule,
  DataListModule,
];

@NgModule({
  imports: [CommonModule, ...MODULES],
  exports: [...MODULES],
})

export class ComponentsModule {
}
