import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  Renderer2,
  HostBinding,
  HostListener,
  OnInit,
  AfterViewChecked
} from '@angular/core';
import { Subject } from 'rxjs';

@Directive({
  selector: '[ngbPosition]'
})
export class PositionDirective
  implements OnChanges, AfterViewInit, OnDestroy, OnInit, AfterViewChecked {
  constructor(private element: ElementRef, private renderer: Renderer2) {}

  ngAfterViewInit() {
    this.onClick();
  }

  ngAfterViewChecked() {
    this.onClick();
  }

  ngOnInit() {
    // this.onClick();
  }

  onClick() {
    const ww = window.innerWidth;
    const rect = this.element.nativeElement.getBoundingClientRect();
    const pos = rect.x + rect.width;
    console.log(rect.width);
    if (pos - ww + (rect.width / 2) > 480) {
      this.renderer.addClass(this.element.nativeElement, 'position-extreme');
    }
  }

  ngOnChanges() {}
  ngOnDestroy() {}
}
