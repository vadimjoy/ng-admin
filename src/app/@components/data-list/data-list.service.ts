import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataListService {
  setConfig(config) {
    return Object.assign(
      {
        mode: 'external',
        attr: {
          class:
            'slds-table slds-table_bordered slds-table_cell-buffer slds-table_striped slds-table_col-bordered'
        },
        rowClassFunction: () => {
          return 'slds-text-title';
        },
        actions: {
          columnTitle: '',
          position: 'right',
          width: '10px'
        },
        add: {
          addButtonContent: '<i class="fa fa-plus data-list__icon"></i>',
          createButtonContent: '<i class="fa fa-check data-list__icon"></i>',
          cancelButtonContent: '<i class="fa fa-close data-list__icon"></i>'
        },
        edit: {
          editButtonContent: '<i class="fa fa-pencil data-list__icon"></i>',
          saveButtonContent: '<i class="fa fa-floppy-o data-list__icon"></i>',
          cancelButtonContent: '<i class="fa fa-close data-list__icon"></i>'
        },
        delete: {
          deleteButtonContent: '<i class="fa fa-trash-o data-list__icon"></i>',
          confirmDelete: true
        },
      },
      config
    );
  }
}
