# Data-list

Based on: `https://github.com/akveo/ng2-smart-table`

## Use

`<component>.html`

```html
<ngb-data-list [url]="'api/data-list-data'" [config]="settings" (add)="onCreateNew()"></ngb-data-list>
```

`<component>.ts`

```js
import { Component, OnInit } from '@angular/core';
import { TableData, TableSettings } from './service';

@Component({
  selector: 'ngb-dev-data',
  templateUrl: 'dev-data.component.html'
})
export class DevDataComponent implements OnInit {
  data: any[];
  settings: any;
  form = new FormView();
  formElements: any;

  ngOnInit() {
    this.data = TableData;
    this.settings = TableSettings;
  }

  onCreateNew() {
    console.log('Click on add-button!');
  }
}
```

### Options

| attribute | type   | description             |
| --------- | ------ | ----------------------- |
| [url]     | string | server api-route (REST) |
| [config]  | object | ng2-smart-table config  |
| (add)     | event  | add button callback     |

## Config

```js
export const TableSettings = {
  mode: 'external',
  columns: {
    id: {
      title: 'ID',
      filter: {
        type: 'list',
        config: {
          selectText: 'Select...',
          list: [
            { value: 1, title: 'Test' },
            { value: 2, title: 'Test1' },
            { value: 3, title: 'Test2' }
          ]
        }
      }
    },
    content: {
      title: 'Content',
      editor: {
        type: 'textarea'
      },
      filter: {
        type: 'completer',
        config: {
          completer: {
            data: TableData,
            searchFields: 'content',
            titleField: 'content',
            placeholder: 'Typing...'
          }
        }
      }
    },
    passed: {
      title: 'Passed',
      filter: {
        type: 'checkbox',
        config: {
          true: 'Yes',
          false: 'No',
          resetText: 'clear'
        }
      }
    }
  }
};
```
