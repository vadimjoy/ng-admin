import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { HttpClient } from '@angular/common/http';
import { DataListService } from './data-list.service';
import { DataService } from '../data.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'ngb-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.css']
})
export class DataListComponent implements OnInit, OnChanges {
  source: ServerDataSource;
  @Input() url: string;
  @Input() config: any;
  @Output() add: EventEmitter<any> = new EventEmitter<any>();
  settings: any;
  private dataListChange = new Subject();

  constructor(
    private http: HttpClient,
    private srv: DataListService,
    private server: DataService
  ) {}

  ngOnChanges(e) {
    this.dataListChange.next(e);
  }

  onEdit() {
    alert('Show edit form');
  }

  onCreate() {
    this.add.emit();
  }

  onDelete() {
    alert('Show delete form');
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  ngOnInit() {
    const init = () => {
      this.source = new ServerDataSource(this.http, { endPoint: this.url });
      this.settings = this.srv.setConfig(this.config);
    };
    if (this.settings) {
      init();
    } else {
      this.dataListChange
        .filter((change: SimpleChange) => change['config'])
        .subscribe(() => {
          init();
        });
    }
  }
}
