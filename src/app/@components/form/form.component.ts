import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChange
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { BaseFormElement, FormService } from './form.service';
import { NotificationsService } from '../notifications/notifications.service';
import { DataService } from '../data.service';

@Component({
  selector: 'ngb-form-element',
  templateUrl: './elements.component.html',
  styleUrls: ['./form.component.scss']
})

export class FormElementComponent {
  @Input() element: any;
  @Input() form: FormGroup;
  @Input() error?: any;
}

@Component({
  selector: 'ngb-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, OnChanges {
  @Input() name = 'Form';
  @Input() elements: BaseFormElement[];
  @Input() url: any;
  @Input() layout: string;
  @Input() isPanel = false;
  @Input() show = false;
  @Input() edit?: any;
  @Output() showChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() created: EventEmitter<any> = new EventEmitter<any>();
  @Output() updated: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleted: EventEmitter<any> = new EventEmitter<any>();
  form: FormGroup;
  layoutClass: string;
  error: Object;
  sending: boolean;
  formElements: BaseFormElement[] = [];
  translate: Object = {
    submit: 'Submit',
    cancel: 'Cancel'
  };
  private create = true;
  private formChange = new Subject();

  constructor(
    private srv: FormService,
    private notifications: NotificationsService,
    private server: DataService
  ) {}

  ngOnChanges(e) {
    this.formChange.next(e);
  }

  changeFormElements() {
    this.formElements = this.srv.getElements(this.elements);
    this.form = this.srv.toFormGroup(this.formElements);
  }

  ngOnInit() {
    const init = () => {
      this.onCreate();
      this.setLayoutClass();
      if (this.edit) {
        this.onFill();
      }
      this.formChange
        .filter((change: SimpleChange) => change['edit'])
        .subscribe(change => {
          this.form.reset();
          if (change['edit'].currentValue !== null) {
            this.onFill();
            this.create = false;
          } else {
            this.changeFormElements();
            this.form.reset();
            this.create = true;
          }
          this.error = null;
        });
      this.formChange
        .filter((change: SimpleChange) => change['layout'])
        .subscribe(() => {
          this.setLayoutClass();
        });
    };
    if (this.elements) {
      init();
    } else {
      this.formChange
        .filter((change: SimpleChange) => change['elements'])
        .subscribe(() => {
          init();
        });
    }
    this.server.translate
      .filter(translate => translate['form'])
      .subscribe(translate => {
        this.translate = translate.form;
      });
  }

  onCreate() {
    this.srv.options(this.name, `${this.url}`).subscribe(result => {
      this.srv.changeOptions(result);
      this.changeFormElements();
    });
  }

  onFill() {
    this.srv.get(this.name, this.url, this.edit).subscribe(result => {
      if (result && typeof result === 'object') {
        this.formElements = this.srv.getElements(
          this.srv.fillForm(this.elements, result)
        );
        this.form.patchValue(result);
      }
    });
  }

  onSubmit() {
    this.sending = true;
    if (this.create) {
      this.srv.add(this.name, this.url, this.form.value).subscribe(result => {
        this.handleResult(result, this.created);
      });
    } else {
      this.srv
        .update(this.name, `${this.url}/${this.edit}`, this.form.value)
        .subscribe(result => {
          this.handleResult(result, this.updated);
        });
    }
  }

  onDelete(): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.srv
        .delete(this.name, `${this.url}/${this.edit}`)
        .subscribe(result => {
          if (result['success']) {
            this.ngOnChanges({
              edit: new SimpleChange(this.edit, null, false)
            });
            this.deleted.emit();
          }
        });
    }
  }

  setLayoutClass() {
    switch (this.layout) {
      case 'stacked':
        this.layoutClass = 'slds-form_stacked';
        break;
      case 'horizontal':
        this.layoutClass = 'slds-form_horizontal';
        break;
      default:
        this.layoutClass = 'slds-form_stacked';
        break;
    }
  }

  hide() {
    this.show = false;
    this.showChange.emit(this.show);
  }

  private handleResult(result, event?) {
    this.error = result.error && result.error.errors ? result.error.errors : {};
    if (result['success']) {
      event.emit();
    }
  }
}
