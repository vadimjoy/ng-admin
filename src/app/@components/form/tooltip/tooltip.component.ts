import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngb-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent {
  @Input() element;
  constructor() { }
}
