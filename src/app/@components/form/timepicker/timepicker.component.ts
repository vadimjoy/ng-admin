import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngb-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss']
})
export class TimepickerComponent  {
  @Input() options: Array<string>;
  @Input() selected: string;
  @Output() timeChange: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  private setSelected(option) {
    this.selected = option;
    this.timeChange.emit(option);
  }
}
