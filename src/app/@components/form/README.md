# Data-list

Based on: `https://github.com/ng-lightning/ng-lightning`

## Use

`<component>.html`

```html
<ngb-form [name]="'Example form'" [elements]="elements" [url]="'api/form'" [isPanel]="form.panel" [(show)]="form.show" [layout]="form.layout" [edit]="form.edit">
    <!-- Only for panel-view form -->
    <span form-title>Edit form</span>
    <span form-sub-title>Jun 18</span>
    <!-- Only for default-view form -->
    <div form-buttons class="slds-grid slds-grid_align-left slds-m-top_small" [class.slds-grid_align-end]="form.layout==='horizontal'">
      <button type="submit" class="slds-button slds-button_brand">Submit</button>
    </div>
  </ngb-form>
```

`<component>.ts`

```js
import { Component, OnInit } from '@angular/core';
import { FormElements } from './form';
import { FormView } from '../../../@components/form/form.service';

@Component({
  selector: 'ngb-dev-forms',
  templateUrl: 'dev-forms.component.html',
  styleUrls: ['dev-forms.component.scss']
})
export class DevFormsComponent implements OnInit {
  elements: any[];
  form = new FormView();

  ngOnInit() {
    this.elements = FormElements;
  }
}
```

### Options

| attribute    | type                  | description                                             |
| ------------ | --------------------- | ------------------------------------------------------- |
| [name]       | string                | form name (for the log of console)                      |
| [elements]   | array                 | example below: FormElements                             |
| [url]        | string                | api/form (REST)                                         |
| [isPanel]    | boolean               | set form view (by default on the page or hiding panel)  |
| [(show)]     | boolean               | show/hide panel (if isPanel=true)                       |
| [layout]     | string                | horizontal or stacked                                   |
| [edit]       | string/number or null | null - create new, string/number - edit (id or any url) |
| (showChange) | event                 | event after show/hide                                   |
| (created)    | event                 | event after creating                                    |
| (updated)    | event                 | event after updating                                    |
| (deleted)    | event                 | event after deleting                                    |

## Data structure

### Form elements

```js
const locale = {
  calendar: {
    monthNames: [
      'Январь',
      'Февраль',
      'Март',
      'Апрель',
      'Май',
      'Июнь',
      'Июль',
      'Август',
      'Сентябрь',
      'Октябрь',
      'Ноябрь',
      'Декабрь'
    ],
    dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    dayNamesLong: [
      'Воскресенье',
      'Понедельник',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота'
    ],
    firstDayOfWeek: 1
  }
};

export const FormElements = [
  {
    /**
     * value: string
     */
    key: 'text',
    label: 'Name input',
    controlType: 'text',
    order: 0,
    required: true,
    tooltip: 'Text tooltip'
  },
  {
    /**
     * value: text
     */
    key: 'textarea',
    label: 'Textarea',
    controlType: 'textarea',
    order: 1,
    required: true,
    tooltip: 'Textarea tooltip'
  },
  {
    /**
     * value: key (array of keys)
     * options: { key:any, value:any }
     */
    key: 'select',
    label: 'Select',
    controlType: 'select',
    order: 2,
    required: true,
    tooltip: 'Select tooltip'
  },
  {
    /**
     * value: array of keys
     * options: { key:any, value:any }
     */
    key: 'select2',
    label: 'Select 2 multiple',
    placeholder: 'Select items',
    controlType: 'select2',
    order: 3,
    required: true,
    multiple: true,
    note: 'Selected items: ',
    tooltip: 'Select 2 tooltip'
  },
  {
    /**
     * value: key
     * options: { key:any, value:any }
     */
    key: 'select2s',
    label: 'Select 2 single',
    placeholder: 'Select items',
    controlType: 'select2',
    order: 4,
    required: true,
    multiple: false,
    note: 'Selected items: ',
    tooltip: 'Select 2s tooltip'
  },
  {
    /**
     * value: boolean
     */
    key: 'checkbox',
    label: 'Checkbox',
    controlType: 'checkbox',
    order: 5,
    required: true,
    tooltip: 'Checkbox tooltip'
  },
  {
    /**
     * value: boolean
     */
    key: 'toggle',
    label: 'Toggle',
    controlType: 'toggle',
    order: 6,
    required: true,
    tooltip: 'Toggle tooltip'
  },
  {
    /**
     * value: array of keys
     * options: { key:any, value:any }
     */
    key: 'radiogroup',
    label: 'Radiogroup',
    controlType: 'radiogroup',
    order: 7,
    required: true,
    tooltip: 'Radiogroup tooltip'
  },
  {
    /**
     * value: array of keys
     * options: { key:any, value:any }
     */
    key: 'radiogroup_alt',
    label: 'Radiogroup alternate',
    controlType: 'radiogroup-alt',
    order: 8,
    required: true,
    tooltip: 'Radiogroup alternate tooltip'
  },
  {
    /**
     * value: array of keys
     * options: { key:any, value:any }
     */
    key: 'checkboxgroup',
    label: 'Checkboxgroup',
    controlType: 'checkboxgroup',
    order: 9,
    required: true,
    tooltip: 'Checkboxgroup tooltip'
  },
  {
    /**
     * value: array of keys
     * options: { key:any, value:any }
     */
    key: 'checkboxgroup_alt',
    label: 'Checkboxgroup alternate',
    controlType: 'checkboxgroup-alt',
    order: 10,
    required: true,
    tooltip: 'Checkboxgroup alternate tooltip'
  },
  Object.assign(
    {
      /**
       * value: Date
       */
      key: 'datepicker',
      label: 'Datepicker',
      controlType: 'datepicker',
      order: 10,
      required: true,
      tooltip: 'Datepicker tooltip'
    },
    locale.calendar
  ),
  {
    /**
     * value: key
     * options: { key:any, value:any }
     */
    key: 'timepicker',
    label: 'Timepicker',
    controlType: 'timepicker',
    order: 12,
    required: true,
    tooltip: 'Timepicker tooltip'
  },
  {
    /**
     * value: number
     */
    key: 'number',
    label: 'Number',
    controlType: 'number',
    order: 14,
    required: true,
    min: 0,
    max: 9,
    step: 0.5,
    tooltip: 'Number tooltip'
  }
];
```

### Options (for multiple select elements)

**URL (example):** `/api/form`  
**Request Method:** OPTIONS

```js
{
  checkboxgroup: [
    { key: '1', value: 'Item 1' },
    { key: '2', value: 'Item 2' },
    { key: '3', value: 'Item 3' }
  ];
  checkboxgroup_alt: [
    { key: '1', value: 'Item 1' },
    { key: '2', value: 'Item 2' },
    { key: '3', value: 'Item 3' }
  ];
  radiogroup: [
    { key: '1', value: 'Item 1' },
    { key: '2', value: 'Item 2' },
    { key: '3', value: 'Item 3' }
  ];
  radiogroup_alt: [
    { key: '1', value: 'Item 1' },
    { key: '2', value: 'Item 2' },
    { key: '3', value: 'Item 3' }
  ];
  select: [
    { key: '1', value: 'Item 1' },
    { key: '2', value: 'Item 2' },
    { key: '3', value: 'Item 3' }
  ];
  select2: [
    { key: '1', value: 'Item 1' },
    { key: '2', value: 'Item 2' },
    { key: '3', value: 'Item 3' }
  ];
  select2s: [
    { key: '1', value: 'Item 1' },
    { key: '2', value: 'Item 2' },
    { key: '3', value: 'Item 3' }
  ];
  timepicker: [
    { key: '1', value: '9:00' },
    { key: '2', value: '14:00' },
    { key: '3', value: '16:00' }
  ];
}
```

### Data for filling

**URL (example):** `/api/form`  
**Request Method:** POST (if new), PUT (if edit)

```js
{
  id: 6;
  checkbox: '1';
  checkboxgroup: ['1'];
  checkboxgroup_alt: ['1'];
  datepicker: '2018-04-30T19:00:00.000Z';
  number: '1';
  radiogroup: '1';
  radiogroup_alt: '1';
  select: '1';
  select2: ['1'];
  select2s: '1';
  text: 'example text';
  textarea: 'example text';
  timepicker: '1';
  toggle: '1';
}
```

## Adding a new item

Into `@components/form/form.service.ts`

1.  Create class with control logic
2.  Add new class in switch-case (getElements method)

Into `@components/form/elements.conponent.html`
add markup by specifying the condition ngSwitchCase

Add new element to data structure object (Form controls - first in this manual) for the creating
