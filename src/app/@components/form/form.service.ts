import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { DataService } from '../data.service';

export class FormView {
  show: boolean;
  panel: boolean;
  layout: string;
  edit: any;

  constructor(
    options: {
      show?: boolean;
      panel?: boolean;
      layout?: string;
      edit?: any;
    } = {}
  ) {
    this.show = options['show'] || false;
    this.panel = options['panel'] || false;
    this.layout = options['layout'] || 'stacked';
    this.edit = options['edit'] || null;
  }

  /**
   * Toggle panel form
   * @returns {boolean}
   */
  toggle() {
    return (this.show = !this.show);
  }

  /**
   * Show panel form
   * @returns {boolean}
   */
  up() {
    return (this.show = true);
  }

  /**
   * Hide panel form
   * @returns {boolean}
   */
  down() {
    return (this.show = false);
  }

  /**
   * Switch form view
   * @returns {boolean}
   */
  switchView() {
    return (this.panel = !this.panel);
  }

  /**
   * Set view as panel
   * @returns {boolean}
   */
  viewAsPanel() {
    return (this.panel = true);
  }

  /**
   * Set view as default
   * @returns {boolean}
   */
  viewAsDefault() {
    return (this.panel = false);
  }

  /**
   * Switch form layout
   * @returns {any}
   */
  switchLayout() {
    switch (this.layout) {
      case 'stacked':
        return (this.layout = 'horizontal');
      case 'horizontal':
        return (this.layout = 'stacked');
      default:
        return (this.layout = 'stacked');
    }
  }

  /**
   * Set form layout as staked
   * @returns {string}
   */
  stacked() {
    return (this.layout = 'stacked');
  }

  /**
   * Set form layout as horizontal
   * @returns {string}
   */
  horizontal() {
    return (this.layout = 'horizontal');
  }

  /**
   * Fill form (listening in component)
   */
  get(data) {
    return (this.edit = data || 0);
  }

  /**
   * Clear form (listening in component)
   */
  clear() {
    return (this.edit = null);
  }
}

export abstract class BaseFormElement {
  /**
   * Value
   * @type {*}
   * @memberof BaseFormElement
   */
  value: any;
  /**
   * Name
   * @type {string}
   * @memberof BaseFormElement
   */
  key: string;
  /**
   * Label
   * @type {string}
   * @memberof BaseFormElement
   */
  label: string;
  /**
   * Input type
   * @type {string}
   * @memberof BaseFormElement
   */
  controlType: string;

  constructor(
    options: {
      value?: any;
      key?: string;
      label?: string;
      controlType?: string;
    } = {}
  ) {}
}

/**
 * Base form element
 */
class FormElement implements BaseFormElement {
  value: any;
  key: string;
  note: string;
  label: string;
  controlType: string;
  order: number;
  required: boolean;
  tooltip: string;
  private tooltipOpen: boolean;

  constructor(
    options: {
      value?: any;
      key?: string;
      note?: string;
      label?: string;
      controlType?: string;
      order?: number;
      required?: boolean;
      tooltip?: string;
    } = {}
  ) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.required = !!options.required;
    this.order = options.order || 1;
    this.controlType = options.controlType || '';
    this.note = options.note || '';
    this.tooltip = options.tooltip;
  }
}

/**
 * Text
 */
class FormTextElement extends FormElement {
  placeholder: string;

  constructor(options: {} = {}) {
    super(options);
    this.placeholder = options['placeholder'] || '';
  }
}

/**
 * Number
 */
class FormNumberElement extends FormTextElement {
  min: number;
  max: number;
  step: number;

  constructor(options: {} = {}) {
    super(options);
    this.value = options['value'] || 0;
    this.min = options['min'] !== undefined ? options['min'] : null;
    this.max = options['max'] !== undefined ? options['max'] : null;
    this.step = parseFloat(options['step']) || 1;
  }

  increment(form, key) {
    if (!this.max === undefined || this.value < this.max) {
      this.value = this.value + this.step;
      form.get(this.key).setValue(this.value);
    }
  }

  decrement(form, key) {
    if (this.min === undefined || this.value > this.min) {
      this.value = this.value - this.step;
      form.get(this.key).setValue(this.value);
    }
  }
}

/**
 * Radiogroup
 */
class FormRadioGroup extends FormTextElement {
  options: { key: string; value: string }[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
    if (options['value'] !== undefined && options['options'].length) {
      this.setOptionKeyType();
    }
  }

  setOptionKeyType() {
    let v_type: any;

    typeof this.value !== 'object'
      ? (v_type = typeof this.value)
      : this.value.map(key => (v_type = typeof key));

    this.options.map((opt: any) => {
      if (typeof opt.key !== v_type) {
        if (v_type === 'string') {
          opt.key = opt.key.toString();
        }
        if (v_type === 'number') {
          opt.key = parseFloat(opt.key);
        }
      }
      return opt;
    });
  }
}

/**
 * Select
 */
class FormSelect extends FormRadioGroup {
  multiple: boolean;
  open: boolean;

  constructor(options: {} = {}) {
    super(options);
    this.multiple = !!options['multiple'];
    this.open = !!options['open'];
  }
}

/**
 * Select2
 */
class FormSelect2 extends FormSelect {
  selected: { key: string; value: string }[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.selected = [];
    if (options['value'] && options['options']) {
      const value = options['value'];
      options['options'].forEach(el => {
        if (value.indexOf(el.key) !== -1) {
          if (typeof value === 'object') {
            this.selected.push(el);
          }
          if (typeof value === 'string') {
            this.selected = el;
          }
        }
      });
    }
  }

  onChangePick(form: FormGroup) {
    if (this.multiple) {
      form.get(this.key).setValue(
        this.selected.map(el => {
          return el.key;
        })
      );
      this.selected.sort((a: any, b: any) => a.key - b.key);
    } else {
      form.get(this.key).setValue(this.selected['key']);
    }
  }

  removePill(el) {
    this.selected = this.selected.filter(value => {
      return value !== el;
    });
  }
}

/**
 * Checkboxgroup
 */
class FormCheckboxgroup extends FormRadioGroup {
  value: Array<string>;

  constructor(options: {} = {}) {
    super(options);
    this.value = options['value'] || [];
  }

  check(key) {
    return this.value.indexOf(key) !== -1;
  }

  onChange(form, control, key) {
    const index = this.value.indexOf(key);
    index === -1 ? this.value.push(key) : this.value.splice(index, 1);
    form.get(control).setValue(this.value.sort());
  }
}

/**
 * Datepicker
 */
class FormDatepicker extends FormTextElement {
  isOpen: boolean;
  monthNames?: Array<string>;
  dayNamesShort?: Array<string>;
  dayNamesLong?: Array<string>;
  firstDayOfWeek?: number;
  private date: Date;
  private display: string;

  constructor(options: {} = {}) {
    super(options);
    this.isOpen = !!options['isOpen'];
    this.monthNames = options['monthNames'] || [
      'January',
      'February',
      'March',
      'April',
      'May',
      'Jun',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];
    this.dayNamesShort = options['dayNamesShort'] || [
      'Sun',
      'Mon',
      'Tue',
      'Wed',
      'Thu',
      'Fri',
      'Sat'
    ];
    this.dayNamesLong = options['dayNamesLong'] || [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday'
    ];
    this.firstDayOfWeek = options['firstDayOfWeek'] || 0;
    this.display = options['value'] ? this.formatDate(options['value']) : '';
    this.date = options['value']
      ? this.formatDate(options['value'], true)
      : null;
  }

  formatDate(value: string, clear?: boolean): any {
    const date = new Date(value);
    if (!clear) {
      return date.toLocaleDateString('ru-RU');
    } else {
      return date;
    }
  }

  onChangeDate(value: string, form: FormGroup, key: string) {
    const date = this.formatDate(value);
    if (this.display !== date) {
      this.display = date;
      form.get(this.key).setValue(value);
    }
  }
}

/**
 * Timepicker
 */
class FormTimepicker extends FormRadioGroup {
  isOpen: boolean;
  private display: string;

  constructor(options: {} = {}) {
    super(options);
    this.isOpen = !!options['isOpen'];
    this.options = options['options'] || [];
    if (options['value'] && options['options'].length) {
      options['options'].forEach(el => {
        if (options['value'] === el.key) {
          this.display = el.value;
        }
      });
    } else {
      this.display = '';
    }
  }

  onChange(option: string, form: FormGroup) {
    this.display = option['value'];
    form.get(this.key).setValue(option['key']);
  }
}

/**
 * Service
 */
@Injectable({
  providedIn: 'root'
})
export class FormService {
  opts: Array<any> = [];

  constructor(private server: DataService) {}

  /**
   * Create form from created elements
   * @param elements
   */
  toFormGroup(elements: BaseFormElement[]) {
    const group: any = {};
    elements.forEach(el => {
      group[el.key] = this.toFormControl(el);
    });
    return new FormGroup(group);
  }

  /**
   * Get created form
   * @param {string} name
   * @param {string} url
   * @param id
   * @returns {Observable<BaseFormElement[]>}
   */
  get(name: string, url: string, id: any): Observable<BaseFormElement[]> {
    return this.server.getForm(name, url, id);
  }

  /**
   * Get options
   * @param {string} name
   * @param {string} url
   * @returns {Observable<any>}
   */
  options(name: string, url: string) {
    return this.server.getFormOptions(name, url);
  }

  /**
   * Create new
   * @param {string} name
   * @param {string} url
   * @param data
   * @returns {Observable<any>}
   */
  add(name: string, url: string, data: any) {
    return this.server.postForm(name, url, data);
  }

  update(name: string, url: string, data: any) {
    return this.server.putForm(name, url, data);
  }

  delete(name: string, url: string) {
    return this.server.deleteForm(name, url);
  }

  changeOptions(options) {
    this.opts = options;
  }

  /**
   * Create elements from array
   * @param elements
   */
  getElements(elements: BaseFormElement[]) {
    const form_elements = [];
    elements.forEach(el => {
      if (this.opts && this.opts[el.key]) {
        el['options'] = this.opts[el.key];
      }
      switch (el.controlType) {
        case 'text':
          form_elements.push(new FormTextElement(el));
          break;
        case 'textarea':
          form_elements.push(new FormTextElement(el));
          break;
        case 'number':
          form_elements.push(new FormNumberElement(el));
          break;
        case 'select':
          form_elements.push(new FormSelect(el));
          break;
        case 'select2':
          form_elements.push(new FormSelect2(el));
          break;
        case 'radiogroup':
          form_elements.push(new FormRadioGroup(el));
          break;
        case 'radiogroup-alt':
          form_elements.push(new FormRadioGroup(el));
          break;
        case 'checkboxgroup':
          form_elements.push(new FormCheckboxgroup(el));
          break;
        case 'checkboxgroup-alt':
          form_elements.push(new FormCheckboxgroup(el));
          break;
        case 'datepicker':
          form_elements.push(new FormDatepicker(el));
          break;
        case 'timepicker':
          form_elements.push(new FormTimepicker(el));
          break;
        default:
          form_elements.push(new FormElement(el));
      }
    });
    return this.sortByOrder(form_elements);
  }

  /**
   * Fill patcher
   */
  fillForm(elements, result) {
    const form_elements = [];
    elements.forEach(el => {
      const fillable = Object.assign({}, el);
      const value = result[fillable.key];
      if (value) {
        fillable.value = value;
      }
      form_elements.push(fillable);
    });
    return form_elements;
  }

  /**
   * Helper for create form control
   * @param element
   */
  private toFormControl(el) {
    return el.required
      ? new FormControl(el.value || '', Validators.required)
      : new FormControl(el.value || '');
  }

  /**
   * Sort elements by order
   * @param elements
   */
  private sortByOrder(elements) {
    return elements.sort((a, b) => a.order - b.order);
  }
}
