import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormComponent, FormElementComponent } from './form.component';
import { FormService } from './form.service';
import { NglModule } from 'ng-lightning/ng-lightning';
import { CloseoutDirective } from '../closeout.directive';
import { PositionDirective } from '../position.directive';
import { TimepickerComponent } from './timepicker/timepicker.component';
import { TooltipComponent } from './tooltip/tooltip.component';

@NgModule({
  imports: [
    CommonModule,
    NglModule.forRoot(),
    ReactiveFormsModule
  ],
  declarations: [
    FormComponent,
    FormElementComponent,
    CloseoutDirective,
    PositionDirective,
    TimepickerComponent,
    TooltipComponent
  ],
  exports: [
    FormComponent,
    FormElementComponent
  ],
})

export class FormModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: FormModule,
      providers: [
        FormService
      ],
    };
  }
}
